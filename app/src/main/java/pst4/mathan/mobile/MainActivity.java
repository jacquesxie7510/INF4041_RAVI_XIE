package pst4.mathan.mobile;


import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.View;
import android.widget.Button;


import static pst4.mathan.mobile.R.styleable.View;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


       Button b_browser = (Button)findViewById(R.id.button3);
        b_browser.setOnClickListener( new android.view.View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Notre site web fait dans le cadre du projet web
                String url = "http://cocktailsforlife.hol.es/index.php";

                // attente du clic
                Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
                startActivity(intent);
            }
        });

        Button btnMap = (Button) findViewById(R.id.button);

        btnMap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // attente du clic pour éxecuter la class welcome
                Intent intent2 = new Intent(MainActivity.this, Welcome.class);
                startActivity(intent2);
            }
        });
/*

*/
        // notification
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)

                // image de la notification
                .setSmallIcon(R.mipmap.ic_launcher)
                // titre de la notification
                .setContentTitle("Cocktails for life")
                // texte de la notification
                .setContentText("Bienvenue dans notre application");

            // affichage de la notification
        NotificationManager gestionnaireDeNotification = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        gestionnaireDeNotification.notify(1,notification.build());
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ajoute les entrées de menu_test à l'ActionBar
        getMenuInflater().inflate(R.menu.menu_test, menu);

        return true;
    }
}
