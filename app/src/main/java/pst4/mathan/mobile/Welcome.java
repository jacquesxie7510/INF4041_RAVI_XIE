package pst4.mathan.mobile;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;



public class Welcome extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] values = new String[]{"Mojito - rhum cubain", "pina colada - rhum blanc", "Margarita - tequila",
                "sex on the bitch - vodka" , "soupe de champagne - champagne", "Caipirinha - cachaca" , "Cosmopolitan - vodka" , "Blue Lagon - vodka" };

        MonAdaptateurDeListe adaptateur = new MonAdaptateurDeListe(this, values);
        setListAdapter(adaptateur);

    }
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Toast.makeText(this, "Position : " + position, Toast.LENGTH_LONG).show();
    }


}
