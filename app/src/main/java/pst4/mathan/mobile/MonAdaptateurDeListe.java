package pst4.mathan.mobile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Jacques on 19/12/2016.
 */

public class MonAdaptateurDeListe extends ArrayAdapter<String> {

         // création de la listeview

    private Integer[] tab_images_pour_la_liste = {
            R.drawable.mojito, R.drawable.pina,
            R.drawable.margarita, R.drawable.sex, R.drawable.soupe,R.drawable.caipirinha,R.drawable.cosmopolitan,R.drawable.blue,
    };

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);

        TextView textView = (TextView) rowView.findViewById(R.id.label);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        textView.setText(getItem(position));

        // affichage de l'image par rapport à sa position dans le tableau

        if (convertView == null)
            imageView.setImageResource(tab_images_pour_la_liste[position]);
        else
            rowView = (View) convertView;

        return rowView;
    }

    public MonAdaptateurDeListe(Context context, String[] values) {
        super(context, R.layout.rowlayout, values);
    }
}